// hasta base 32
const TuNumeroGrande_simbolos = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E'
,'F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U'
,'V','W','X','Y','Z'];
class TuNumeroGrande{
    constructor(numero,base){        
        base = parseInt(base);        
        this.base = isNaN(base)?10:base;
        this.A = this._valorAVector(numero,this.base);
        if([2,8,10,16].indexOf(this.base)<0  )
            throw Error("base "+base+" no soportada");
    }
    _valorAVector(variable,base){
        //cualquier valor separado por arrays lo convertimos a base 10
        switch(typeof variable){
            case 'number':
                variable = variable+"";
            case 'string':
                return variable.trim().split('')
                .map((numero)=>{
                    const r = parseInt(numero,base||this.base);
                    if(isNaN(r))
                        throw Error("numero "+numero+" no soportado en base "+base);
                    return r;
                });                        
            case 'object':
                if(variable instanceof Array) 
                    //suponemos que el array es de enteros en base 10
                    return variable;
                else
                    throw Error("solo se puede sumar arrays");
        }
    }
    _multiplicarPorEntero(vector,digito){
        const resultado = [];            
        var acarreo =0;
        for (let index = vector.length-1; index >=0; index--) {
            const a = vector[index] || 0;

            const r = (a*digito)+acarreo;
            resultado.unshift(r%10);
            acarreo = parseInt(r/10);                
            if(index===0){
                while(acarreo>0){
                    resultado.unshift(acarreo%10);
                    acarreo = parseInt(acarreo/10);                        
                }
            }
                //resultado.unshift(acarreo );
        }
        return resultado;
    }
    _multiplicarVectorDecimal(vector1,vector2){
        const resultado = [];
        // const maximo = Math.max(vector1.length,vector2.length);
        // var acarreo =0;
        // for (let index = maximo; index >0; index--) {
        //     const a = vector1[index] || 0;
        //     const b = vector2[index] || 0;
        //     const r = (a*b)+acarreo;
        //     const digito = r%10;
        //     acarreo = parseInt(r/10);
        //     resultado.unshift(digito);
        //     if(index===1 && acarreo>0)
        //         resultado.unshift(acarreo );
        // }
        const limite1 = vector1.length-1;
        const limite2 = vector2.length-1;
        for (let i = limite1; i >=0 ; i--) {
            const digito = vector1[i];
            for(let j = limite2; j>=0;j--){
                const digito2 = vector2[j];
                const r = (digito*digito2);
                const digito3 = resultado[i+j+1] || 0;
                const suma = digito3+r;
                const digito4 = suma%10;
                const acarreo = parseInt(suma/10);
                resultado[i+j+1] = digito4;
                if(acarreo>0){
                    resultado[i+j] = (resultado[i+j]||0)+acarreo;
                }
            }
        }
        return resultado;
    }
    _sumarVectores(vector1,vector2){
        const resultado = [];
        const maximo = Math.max(vector1.length,vector2.length);
        var acarreo =0;
        for (let index = maximo; index >0; index--) {
            const a = vector1[index] || 0;
            const b = vector2[index] || 0;
            //const r = callback(a,b,acarreo);
            const sumatoria = a+b+acarreo;
            const digito = sumatoria%10;
            acarreo = parseInt(sumatoria/10);
            resultado.unshift(digito);
            if(index===1 && acarreo>0)
                resultado.unshift(acarreo );
        }
        return resultado;
    }    
    getDigitos(){
        return this.A;
    }
    getBase(){
        return this.base;
    }
    toBase(nuevaBase){
        
        if(this.base == nuevaBase)
            return this.A;
            // Convertir el número de la base actual a decimal
        let decimal = [];
        decimal.push(0);
        for (let i = 0; i < this.A.length; i++) {
            let acarreo = this.A[i];
            for (let j = 0; j < decimal.length || acarreo !== 0; j++) {
                let valor = decimal[j] || 0;
                let producto = valor * this.base + acarreo;
                decimal[j] = producto % nuevaBase;
                acarreo = Math.floor(producto / nuevaBase);
            }
        }
        const retorno = [];
        while(decimal.length>0){
            retorno.push(TuNumeroGrande_simbolos[decimal.pop()]);
        }
        return retorno;
        // const baseArray = this._valorAVector(newBase);
        // var resultado = [...this.A];
        // var exponentes = [1];
        // for (let i = resultado.length-1; i > 0; i--) {
        //     exponentes = (this._multiplicarPorEntero(exponentes,newBase));
        //     const porcion = this._multiplicarPorEntero(exponentes,baseArray[i]);
        //     resultado = this._sumarVectores(resultado,porcion);
        // }
        // return resultado;
    }
    toArray(){
        return this.A.map((h)=>TuNumeroGrande_simbolos[h]);
    }
    toString(){
        return this.toArray().join('');
    }
    suma(otroNumero,base){        
        this.A.splice(0
            ,this.A.length,
            ...this._sumarVectores(this.A,this._valorAVector(otroNumero,base))
        );
        return this;
    }
    multiplica(otroNumero,base){
        const B = this._valorAVector(otroNumero,base);
        const matriz_sumas = [];
        let ceros = 0;
        for (let i = B.length-1; i >=0 ; i--) {
            //console.log("multiplicador",B[i]);
            //const multiplicador= [B[i]];                
            const linea = this._multiplicarPorEntero(this.A,B[i]);
            for (let j = 0; j < ceros; j++) {
                linea.push(0);
            }
            ceros++;
            matriz_sumas.push(linea);                    
        }
        var actual = matriz_sumas.shift();
        //console.log("0>",actual);
        while(matriz_sumas.length>0){
            const a = matriz_sumas.shift();
            //console.log("a>",a);
            actual = this._sumarVectores(a,actual);
        }
        this.A.splice(0,this.A.length,...actual);
        return this;
    }
    //aqui los metodos
};
//var test = new TuNumeroGrande(20001);
//console.log(test.toBase(16));