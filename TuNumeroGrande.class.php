<?php

$TuNumeroGrande_simbolos = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

class TuNumeroGrande {
    const TuNumeroGrande_simbolos= ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
    private $A;
    private $base;

    public function __construct($numero, $base) {
        $base = (int)$base;
        $this->base = !is_nan($base) ? $base : 10;
        $this->A = $this->_valorAVector($numero, $this->base);

        if (!in_array($this->base, [2, 8, 10, 16])) {
            throw new Exception("base ".$base." no soportada");
        }
    }

    private function _valorAVector($variable, $base) {
        switch(gettype($variable)) {
            case 'integer':
                $variable = strval($variable);
            case 'string':
                return array_map(function($numero) use ($base) {
                    $r = intval($numero, $base !== null ? $base : $this->base);
                    if (is_nan($r)) {
                        throw new Exception("numero ".$numero." no soportado en base ".$base);
                    }
                    return $r;
                }, str_split(trim($variable)));
            case 'array':
                if (is_array($variable)) {
                    return $variable;
                } else {
                    throw new Exception("solo se puede sumar arrays");
                }
        }
    }

    private function _multiplicarPorEntero($vector, $digito) {
        $resultado = [];
        $acarreo = 0;
        for ($i = count($vector) - 1; $i >= 0; $i--) {
            $a = isset($vector[$i]) ? $vector[$i] : 0;
            $r = ($a * $digito) + $acarreo;
            array_unshift($resultado, $r % 10);
            $acarreo = intval($r / 10);
            if ($i === 0) {
                while ($acarreo > 0) {
                    array_unshift($resultado, $acarreo % 10);
                    $acarreo = intval($acarreo / 10);
                }
            }
        }
        return $resultado;
    }

    private function _multiplicarVectorDecimal($vector1, $vector2) {
        $resultado = [];
        $limite1 = count($vector1) - 1;
        $limite2 = count($vector2) - 1;
        for ($i = $limite1; $i >= 0; $i--) {
            $digito = $vector1[$i];
            for ($j = $limite2; $j >= 0; $j--) {
                $digito2 = $vector2[$j];
                $r = $digito * $digito2;
                $digito3 = isset($resultado[$i + $j + 1]) ? $resultado[$i + $j + 1] : 0;
                $suma = $digito3 + $r;
                $digito4 = $suma % 10;
                $acarreo = intval($suma / 10);
                $resultado[$i + $j + 1] = $digito4;
                if ($acarreo > 0) {
                    $resultado[$i + $j] = isset($resultado[$i + $j]) ? $resultado[$i + $j] + $acarreo : $acarreo;
                }
            }
        }
        return $resultado;
    }

    private function _sumarVectores($vector1, $vector2) {
        $resultado = [];
        $maximo = max(count($vector1), count($vector2));
        $acarreo = 0;
        for ($index = $maximo; $index > 0; $index--) {
            $a = isset($vector1[$index]) ? $vector1[$index] : 0;
            $b = isset($vector2[$index]) ? $vector2[$index] : 0;
            $sumatoria = $a + $b + $acarreo;
            $digito = $sumatoria % 10;
            $acarreo = intval($sumatoria / 10);
            array_unshift($resultado, $digito);
            if ($index === 1 && $acarreo > 0) {
                array_unshift($resultado, $acarreo);
            }
        }
        return $resultado;
    }

    public function getDigitos() {
        return $this->A;
    }

    public function getBase() {
        return $this->base;
    }

    public function toBase($nuevaBase) {
        if ($this->base == $nuevaBase) {
            return $this->A;
        }

        $decimal = [0];
        for ($i = 0; $i < count($this->A); $i++) {
            $acarreo = $this->A[$i];
            for ($j = 0; $j < count($decimal) || $acarreo !== 0; $j++) {
                $valor = isset($decimal[$j]) ? $decimal[$j] : 0;
                $producto = $valor * $this->base + $acarreo;
                $decimal[$j] = $producto % $nuevaBase;
                $acarreo = intval($producto / $nuevaBase);
            }
        }

        $retorno = [];
        while (count($decimal) > 0) {
            array_push($retorno,  TuNumeroGrande::TuNumeroGrande_simbolos[array_pop($decimal)]);
        }
        return $retorno;
    }

    public function toArray() {
        return array_map(function($h)  {
            return  TuNumeroGrande::TuNumeroGrande_simbolos[$h];
        }, $this->A);
    }

    public function toString() {
        return implode('', $this->toArray());
    }

    public function suma($otroNumero, $base) {
        $this->A = $this->_sumarVectores($this->A, $this->_valorAVector($otroNumero, $base));
        return $this;
    }

    public function multiplica($otroNumero, $base) {
        $B = $this->_valorAVector($otroNumero, $base);
        $matriz_sumas = [];
        $ceros = 0;
        for ($i = count($B) - 1; $i >= 0; $i--) {
            $linea = $this->_multiplicarPorEntero($this->A, $B[$i]);
            for ($j = 0; $j < $ceros; $j++) {
                array_push($linea, 0);
            }
            $ceros++;
            array_push($matriz_sumas, $linea);
        }
        $actual = array_shift($matriz_sumas);
        while (count($matriz_sumas) > 0) {
            $a = array_shift($matriz_sumas);
            $actual = $this->_sumarVectores($a, $actual);
        }
        $this->A = $actual;
        return $this;
    }
}

// Ejemplo de uso
//$test = new TuNumeroGrande(20001, 10);
//echo implode('', $test->toBase(16)); // Imprime el resultado en base 16

